import {gql} from 'apollo-boost'

const addUserMutation = gql`
	mutation ($name: String!, $email: String!, $userName: String!, $password: String!) {
		addUser(name: $name, email: $email, userName: $userName, password: $password){
			name
			email
			userName
		}
	}
`
const deleteUserMutation = gql`
	mutation($id: ID!){
		deleteUser(id: $id){
			name
			email
			userName
		}
	}
`

const loginMutation = gql`
    mutation ($email: String!, $password: String!) {
        login(email: $email, password: $password) {
        	id
            name
            email
            userName
            password
            token
        }
    }
`
const addTransactionMutation = gql`
	mutation($eventDate: String!){
		addTransaction(eventDate: $eventDate){
			eventDate
		}
	}
`
const deleteTransactionMutation = gql`
	mutation($id: ID!){
		deleteTransaction(id: $id){
			eventDate
		}
	}
`
const updateTransactionMutation = gql`
	mutation($id: ID!, $eventDate: String!){
		updateTransaction(id: $id, eventDate: $eventDate){
			id
			eventDate
		}
	}
`

const updateUserMutation = gql`
	mutation($id: ID!, $name: String!, $email: String!, $userName: String!, $password: String!){
		updateUser(id: $id, name: $name, email: $email, userName: $userName, password: $password){
			id
			name
			email
			userName
			password
		}
	}
`

export {
	addUserMutation,
	loginMutation,
	addTransactionMutation,
	deleteTransactionMutation,
	updateTransactionMutation,
	deleteUserMutation,
	updateUserMutation
}
