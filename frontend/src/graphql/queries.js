import {gql} from 'apollo-boost'

const getUsersQuery = gql`
{
	users{
		id
		name
		email
		userName
		password
	}
}
`

const getTransactionsQuery = gql`
{
	transactions{
		id
		eventDate
	}
}
`
const getSpecificTransaction = gql`
	query($id: ID!){
		transaction(id:$id){
			id
		}
	}
`

const getSpecificUser = gql`
	query($id: ID!){
		user(id:$id){
			id
			name
			email
			userName
			password
		}
	}
`

export {
	getTransactionsQuery,
	getSpecificTransaction,
	getUsersQuery,
	getSpecificUser
}