import React from 'react'
import TransactionUpdateForm from './TransactionUpdateForm'
import {graphql} from 'react-apollo'
import {flowRight as compose} from 'lodash'
import {getSpecificTransaction} from '../graphql/queries'
import {updateTransactionMutation} from '../graphql/mutations'

const TransactionUpdatePage = (props) => {
	const transaction = props.getSpecificTransaction.transaction
	// console.log(props.getSpecificTransaction)
	const updateTransaction = (e, updateOnTransaction)=>{
		e.preventDefault()

		props.updateTransactionMutation({
			variables: updateOnTransaction
		}).then(()=>{
			window.location.href='/transaction'
		})
	}

	let returnTransactionForm = (<h1>Loading...</h1>)
	if (transaction != undefined) {
		returnTransactionForm = <TransactionUpdateForm transaction={transaction} updateTransaction={updateTransaction}/>
	}

	return(

		<div className="container">
			<div className="row" style={{marginTop: '20%'}}>
				<div className="col-md-3 mx-auto">
					{returnTransactionForm}
				</div>
			</div>
		</div>

	)
}

export default compose(
	graphql(getSpecificTransaction, {
		options: (props)=>{
			return{
				variables: {
					id: props.match.params.transactionId
				}
			}
		},

		name: 'getSpecificTransaction'
	}),
	graphql(updateTransactionMutation, {name: 'updateTransactionMutation'})
	)(TransactionUpdatePage)