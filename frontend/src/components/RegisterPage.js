import React, {useState} from 'react'
//we will use the Redirect component, to redirect the client into Login page after registration
import {Redirect} from 'react-router-dom'
/*Swal alert for the alert/notif/verification/confirmation dialog boxes*/
import Swal from 'sweetalert2'
/*Mutations for inserting a user*/
import styles from './mystyle.module.css'
import Footer from './Footer'

import {addUserMutation} from '../graphql/mutations'
import {graphql} from 'react-apollo'

const RegisterPage = (props) => {
	

	const [name, setName] = useState('')
	const [email, setEmail] = useState('')
	const [userName, setUserName] = useState('')
	const [password, setPassword] = useState('')
	const [redirectToLogin, setRedirectToLogin] = useState(false)

		if (redirectToLogin) {
			return <Redirect to='/login?register=true'/>
		}

	const registerNewUser = (e)=> {
		// alert(name + " " + email + " " + userName)
		e.preventDefault()
		/*
			STEP 5.
			import the mutation addUserMutation to register a user. Use this to insert the records on our graphql
			import the graphql from react-apollo
			export the mutation using graphql()
			use sweet alert after a successful registration (line 49)
		*/
			props.addUserMutation({
				variables: {
					name: name,
					email: email,
					userName: userName,
					password: password
				}
			}).then((response)=>{
				// this is the response from the addUserMutation which is the addUser function from the graphql => name, email, username
				const userAdded = response.data.addUser

				// Create a condition that will check the registration of the user.
				 if (userAdded) {
				 	//if the registration is successful we will display below
		            Swal.fire({
		                title: 'Registration Successful',
		                text: 'You will now be redirected to the login.',
		                type: 'success'
		            }).then(() => {
		            	// then we will set the RedirectToLogin into true
		                setRedirectToLogin(true)
		            })
            }else {
            	Swal.fire({
                    title: 'Registration Failed',
                    text: 'The server encountered an error.',
                    type: 'error'
                })
            }

			})
	}

	return(

		<React.Fragment>
		<div className="container-fluid" id={styles.register}>
		<a href="/" style={{color: 'black', fontSize: '1.1rem'}}><i className="fas fa-angle-double-left"></i> Back</a>
			<div className="row">
				<div className="col-md-4 offset-md-2">

					<h2 className={styles.registerheading}>Register</h2>
					<form onSubmit={(e)=> registerNewUser(e)}>
						<div className="form-group">
							Name:
							<input
								required 
								value={name}
								onChange={(e)=> setName(e.target.value)}
								name="fullname"
								type="text" 
								className="form-control"/>
						</div>

						<div className="form-group">
							Email:
							<input
								required
								value={email}
								onChange={(e)=> setEmail(e.target.value)}
								name="email" 
								type="email" 
								className="form-control"/>
						</div>

						<div className="form-group">
							Username:
							<input
								required 
								value={userName}
								onChange={(e)=> setUserName(e.target.value)}
								name="username" 
								type="text" 
								className="form-control"/>
						</div>

						<div className="form-group">
							Password:
							<input
								required
								value={password}
								onChange={(e)=> setPassword(e.target.value)}
								name="fullname" 
								type="password" 
								className="form-control"/>
						</div>
						<button type="submit" className="btn btn-light btn-block rounded-pill border border-dark">Register</button>

						<p className="text-center my-2">Already have an account?</p>
						<p className="text-center">click <a href="/login">here</a> to Login</p>
					</form>
				</div>
			</div>
		</div>

		<Footer/>
		</React.Fragment>
	)
}

export default graphql(addUserMutation, {name: 'addUserMutation'})(RegisterPage)