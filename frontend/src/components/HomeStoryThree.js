import React from 'react'
// import ReactDOM from 'react-dom'
import styles from './mystyle.module.css'

const HomeStoryThree = () => {
	return(
		<div className="container" data-aos="fade-right">
			<div className="row" id={styles.storythree}>
				<div className="col-md-4 offset-md-4">
					<p className={styles.storythreepara}>"I have worked with so many photographers in my whole life and Jpreyes is on the top of them! He is the nicest photographer in the country trust me!"</p>
					<p className={styles.storythreepara2}>"There are really no regrets working with him!"</p>
				</div>
			</div>
		</div>
	)
}

export default HomeStoryThree