import React from 'react'
// import ReactDOM from 'react-dom'
import {Link} from 'react-router-dom'

const TransactionRow = ({
	individualTransaction : { id, eventDate}, deleteTransaction
})=>{
	return(
		<tr>
			<td>jprp{id}</td>
			<td>{eventDate}</td>
			<td>
				<Link to={"/transaction/update/" + id}>
					<button className="btn btn-dark mr-1 rounded-pill">Change</button>
				</Link>
				<button className="btn btn-danger rounded-pill border border-dark" onClick={()=> deleteTransaction(id)}>Cancel</button>
			</td>
		</tr>
	)
}

export default TransactionRow