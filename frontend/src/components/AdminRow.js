import React from 'react'
import {Link} from 'react-router-dom'

const AdminRow = ({
	individualUser : { id, name, userName, email}, deleteUser
})=>{

	return(
		<tr>
			<td>{name}</td>
			<td>{userName}</td>
			<td>{email}</td>
			<td>
				<button className="btn btn-danger rounded-pill border border-dark" onClick={()=> deleteUser(id)}>Delete</button>
			</td>
		</tr>
	)
}

export default AdminRow