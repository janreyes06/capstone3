import React from 'react'
import styles from './mystyle.module.css'
import UserRow from './UserRow'

const UserList = (props) => {

	let rows = ''
	if (props.user == undefined) {
		rows = (<h2> Loading... </h2>)
	} 
	else {
		rows =  <UserRow user={props.user}/>
	}

	return(
		<div className="row">
			<div className="col-lg-3 col-md-4 mx-auto shadow" style={{padding: '10px', borderRadius: '10px', border: '2px solid #ffeeee'}}>
					  
			    {/*<table className="table table-bordered table-hover shadow text-center">

				  <thead style={{backgroundColor: '#ffeeee'}}>
				    <tr>
				      <th scope="col">Name</th>
				      <th scope="col">Email</th>
				      <th scope="col">Username</th>
				      <th scope="col">Action</th>
				    </tr>
				  </thead>

				  <tbody>
				  	{rows}
				  </tbody>
				</table>*/}
				{rows}
				  
			</div>
				
			
		</div>
	)
}

export default UserList