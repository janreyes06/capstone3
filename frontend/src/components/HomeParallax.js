import React from 'react'
// import ReactDOM from 'react-dom'
import styles from './mystyle.module.css'

const HomeParallax = () => {
	return(
			<div className="container-fluid" id={styles.parallax}>
				<div className="row" id="pricing">
					<div className="col-md-7 mx-auto">
						<h2 className={styles.parallaxheading}>Enjoy the best deal in the country for as low as:</h2>
						<p className={styles.parallaxpara}>Php 20,000 per session</p>
						<p className="text-center">
							<a href="/transaction" className="btn btn-light btn-lg border border-dark rounded-pill">Book Now</a>
						</p>
					</div>
				</div>
			</div>
	)
}

export default HomeParallax