import React from 'react'
// import ReactDOM from 'react-dom'
import styles from './mystyle.module.css'
import Swal from 'sweetalert2'

import TransactionList from './TransactionList'
import TransactionAdd from './TransactionAdd'

import HomeParallax from './HomeParallax'
import Footer from './Footer'
import AppNavBar from './AppNavBar'

import {graphql} from 'react-apollo'
import {flowRight as compose} from 'lodash'
import {getTransactionsQuery} from '../graphql/queries'
import {addTransactionMutation, deleteTransactionMutation} from '../graphql/mutations'


const TransactionPage = (props) => {
	
	const data = props.getTransactionsQuery
	
	const addTransaction = (e, newTransaction)=>{
		e.preventDefault()
		
		props.addTransactionMutation({
			variables: newTransaction,
			refetchQueries: [{query: getTransactionsQuery}]
		})
		
		Swal.fire({
		                title: 'Thank You!',
		                text: "We'll be sending you an Email in a bit!",
		                type: 'success'
		            })
	}

	const deleteTransaction = (id)=>{
		// alert(id)
		props.deleteTransactionMutation({
			variables: {id: id},
			refetchQueries: [{query: getTransactionsQuery}]
		})
		Swal.fire({
		                title: ':(',
		                text: "Appointment cancelled",
		                type: 'success'
		            })
	}
	
	return(
		<React.Fragment>

			<AppNavBar/>

			<div className="container-fluid" id={styles.transaction}>
				
				<TransactionAdd addTransaction={addTransaction}/>
			
				<TransactionList transactions={data.transactions} deleteTransaction={deleteTransaction}/>

			
			</div>
				<HomeParallax/>


			<Footer/>




		</React.Fragment>
	)
}

export default compose(
	graphql(getTransactionsQuery, {name: 'getTransactionsQuery'}),
	graphql(addTransactionMutation, {name: 'addTransactionMutation'}),
	graphql(deleteTransactionMutation, {name: 'deleteTransactionMutation'})
)(TransactionPage)