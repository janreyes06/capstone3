import React, {useState} from 'react'
import {Link} from 'react-router-dom'
import styles from './mystyle.module.css'

const UserUpdateForm = ({user, updateUser}) => {

	const [name, setName] = useState(user.name)
	const [email, setEmail] = useState(user.email)
	const [userName, setUserName] = useState(user.userName)
	const [password, setPassword] = useState(user.password)

	let updateOnUser = {
		id: user.id,
		name: name,
		email: email,
		userName: userName,
		password: password
	}

	return(

			<form onSubmit={(e)=> updateUser(e, updateOnUser)}>
			  <h2 className={styles.editcredentials}>Edit Credentials</h2>
			  <div className="form-group"> 
			    <label htmlFor="inputName">Name:</label>
			    <input
			    	value={name}
			    	onChange={(e)=> setName(e.target.value)}
			    	type="text" 
			    	name="name" 
			    	className="form-control" 
			    	id="inputName" 
			    	required
			    	/>
			  </div>

			  <div className="form-group"> 
			    <label htmlFor="inputEmail">Email:</label>
			    <input
			    	value={email}
			    	onChange={(e)=> setEmail(e.target.value)}
			    	type="email" 
			    	name="email" 
			    	className="form-control" 
			    	id="inputEmail" 
			    	required
			    	/>
			  </div>

			  <div className="form-group"> 
			    <label htmlFor="inputUserName">Username:</label>
			    <input
			    	value={userName}
			    	onChange={(e)=> setUserName(e.target.value)}
			    	type="text" 
			    	name="username" 
			    	className="form-control" 
			    	id="inputUserName" 
			    	required
			    	/>
			  </div>

			  <div className="form-group"> 
			    <label htmlFor="inputPassword">Password:</label>
			    <input
			    	onChange={(e)=> setPassword(e.target.value)}
			    	type="password" 
			    	name="password" 
			    	className="form-control" 
			    	id="inputPassword" 
			    	required
			    	/>
			  </div>
			 
			  <button className="btn btn-light border border-dark rounded-pill my-1">Update</button>
			  <Link to={"/user/" + user.id}>
						<button className="btn btn-danger mx-1 my-1 rounded-pill border border-dark">Cancel</button>
					</Link>
			</form>
	)
}

export default UserUpdateForm