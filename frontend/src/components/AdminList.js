import React from 'react'
import styles from './mystyle.module.css'
import AdminRow from './AdminRow'

const AdminList = (props) => {

	let rows = ''
	if (props.users == undefined) {
		rows = (<tr><td><strong> Loading... </strong></td></tr>)
	} 
	else {
		rows =  props.users.map((singleUser)=>{
				  	return	<AdminRow
				  				key={singleUser.id} 
				  				individualUser={singleUser}
				  				deleteUser={props.deleteUser}
				  			/>		
				  	 })
	}


	return(
		
		<div className="row">
			<div className="col-md-8 mx-auto table-responsive">
				
				  	
				  	<div className="row my-2">
				  		<div className="col-md-5">
				  			<p> <i class="fas fa-search"></i> <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search . . ."/></p>
				  		</div>
				  	</div>
				 
				    <table className="table table-bordered table-hover text-center shadow" id="myTable">
					  <thead style={{backgroundColor: '#ffeeee'}}>
					    
					      <th scope="col">Name</th>
					      <th scope="col">Username</th>
					      <th scope="col">Email</th>
					      <th scope="col">Action</th>
					  
					  </thead>
					  <tbody id="myTable">
					  {rows}
					  </tbody>
					</table>
				  	
				</div>
			
		</div>
	)

}

export default AdminList