import React from 'react'
import {graphql} from 'react-apollo'
import {flowRight as compose} from 'lodash'

import AppNavBar from './AppNavBar'
import Footer from './Footer'
import UserUpdateForm from './UserUpdateForm'
import {getSpecificUser} from '../graphql/queries'
import {updateUserMutation} from '../graphql/mutations'

const UserUpdatePage = (props) => {

	const user = props.getSpecificUser.user

	const updateUser = (e, updateOnUser)=>{
		e.preventDefault()

		props.updateUserMutation({
			variables: updateOnUser
		}).then(()=>{
			window.location.href='/user/' + user.id
		})
	}

	let returnUserForm = (<h1>Loading...</h1>)
	if (user != undefined) {
		returnUserForm = <UserUpdateForm user={user} updateUser={updateUser}/>
	}

	return(
		<React.Fragment>

			<AppNavBar/>

			<div className="container">
				<div className="row" style={{marginTop: '5%', marginBottom: '10%'}}>
					<div className="col-md-4 mx-auto shadow" style={{padding: '10px', borderRadius: '10px',border: '2px solid #ffeeee'}}>
						{returnUserForm}
					</div>
				</div>
			</div>

			<Footer/>

		</React.Fragment>
	)
}

export default compose(
	graphql(getSpecificUser, {
		options: (props)=>{
			return{
				variables: {
					id: props.match.params.id
				}
			}
		},

		name: 'getSpecificUser'
	}),
	graphql(updateUserMutation, {name: 'updateUserMutation'})
	)(UserUpdatePage)