import React from 'react'
// import ReactDOM from 'react-dom'
import styles from './mystyle.module.css'
import TransactionRow from './TransactionRow'

const TransactionList = (props) => {

	let rows = ''
	if (props.transactions == undefined) {
		rows = (<tr><td><strong> Loading... </strong></td></tr>)
	} 
	else {
		rows =  props.transactions.map((singleTransaction)=>{
				  	return	<TransactionRow 
				  				key={singleTransaction.id} 
				  				individualTransaction={singleTransaction}
				  				deleteTransaction={props.deleteTransaction}
				  			/>		
				  	 })
	}


	return(
		<div className="row">
			<div className="col-lg-6 offset-lg-1 col-md-10 offset-md-1 table-responsive">
				
				  	<h2 className={styles.appointmentsheading}>Appointments</h2>
				  
				    <table className="table table-bordered text-center">
					  <thead style={{backgroundColor: '#ffeeee'}}>
					    <tr>
					      <th scope="col">Transaction Code</th>
					      <th scope="col">Event Date</th>
					      <th scope="col">Action</th>
					    </tr>
					  </thead>
					  <tbody>
					  {rows}
					  </tbody>
					</table>
				  
				</div>
			
		</div>
	)
}

export default TransactionList