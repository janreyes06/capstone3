import React from 'react'
// import ReactDOM from 'react-dom'
import styles from './mystyle.module.css'

const LandingStoryOne = () => {
	return(
		<div className="container" data-aos="fade-right">
			<h2 className={styles.storiesheading}>Stories</h2>
			<div className="row" id={styles.storyone}>
				<div className="col-md-4 offset-md-4">
					<p className={styles.storyonepara}>"working with Jpreyes is one of the best decision that my husband and I have made. He made our wedding day memory looks so alive and fabulous!"</p>
					<p className={styles.storyonepara2}>"There are really no regrets working with him!"</p>
				</div>
			</div>
		</div>
	)
}

export default LandingStoryOne