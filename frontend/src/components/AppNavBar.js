import React from 'react'
// import ReactDOM from 'react-dom'
import styles from './mystyle.module.css'
import {Link} from 'react-router-dom'

const AppNavBar = () => {
	const logout = () => {
	// removes all key-value pairs that is stored in our localStorage
	localStorage.clear()
	window.location.href = "/"
}

	
			let name = localStorage.getItem('name')
			let a = ''
			if (name == "admin") {
				a = (
						<a className="dropdown-item" href="/admin">Dashboard</a>
					)
			}

			let id = localStorage.getItem('id')
		      		
	return(
		<nav className="navbar navbar-expand-lg navbar-light" id={styles.mynav}>
		  
		  	<button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
		    	<span className="navbar-toggler-icon"></span>
		  	</button>

		  	<div className="collapse navbar-collapse" id="navbarNav">
		    	<ul className="navbar-nav mx-auto">

		      		<li className="nav-item mx-2" id={styles.navitem}>
		        		<a className="nav-link" href="/home">Home</a>
		      		</li>

		      		<li className="nav-item mx-2" id={styles.navitem}>
		        		<a className="nav-link" href="#pricing">Pricing</a>
		      		</li>

		      		<li className="nav-item mx-2" id={styles.navitem}>
		        		<a className="nav-link" href="/transaction">Book Now</a>
		      		</li>
					<li className="nav-item dropdown">
				        <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				          <strong>{name}</strong>
				        </a>
				        <div className="dropdown-menu" aria-labelledby="navbarDropdown">
				        	{
				          		a
				          	}
				          	<a className="dropdown-item" href={"/user/" + id}>My Account</a>
				          	<a className="dropdown-item" onClick={()=> logout()}><i className="fas fa-sign-out-alt"></i> Logout</a>
				          
				        </div>
				    </li>
		    	</ul>
		  	</div>
		</nav>
	)
}

export default AppNavBar