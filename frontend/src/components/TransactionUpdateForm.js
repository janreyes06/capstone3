import React, {useState} from 'react'

const TransactionUpdateForm = ({transaction, updateTransaction})=> {
	console.log(transaction)
	const [eventDate, setEventDate] = useState(transaction.eventDate)
	let updateOnTransaction = {
		id: transaction.id,
		eventDate: eventDate
	}

	return(
		<form onSubmit={(e)=> updateTransaction(e, updateOnTransaction)}>
		
		  <div className="form-group"> 
		    <label htmlFor="inputFirstName">Event Date</label>
		    <input
		    	value={eventDate}
		    	onChange={(e)=> setEventDate(e.target.value)}
		    	type="date" 
		    	name="eventDate" 
		    	className="form-control" 
		    	id="inputEventDate" 
		    	required
		    	/>
		  </div>
		 
		  <button className="btn btn-light border border-dark rounded-pill">Update</button>
		  <a href="/transaction" className="btn btn-danger border border-dark rounded-pill mx-2">Cancel</a>
		</form>
	)
}

export default TransactionUpdateForm