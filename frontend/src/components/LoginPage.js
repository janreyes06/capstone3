import React, {useState} from 'react'
/*Sweet alert*/
import Swal from 'sweetalert2'
/*We'll use Redirect, to redirect the user into the homepage after successfully LoggedIn*/
import { Redirect } from 'react-router-dom'
/*Mutations for Log in*/
import { graphql} from 'react-apollo'
import { loginMutation } from '../graphql/mutations'
import styles from './mystyle.module.css'
import Footer from './Footer'

const LoginPage = (props)=>{
	/*
		STEP 2.
		Create the states for the email and password
		Import the state from react module
	*/

	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
	// state that will check whether to redirect or not.
	const [isRedirected, setIsRedirected] = useState(false)

	// If the state has a value of true, redirect to the ProductsPage
	if (isRedirected) {
        return <Redirect to='/home'/>
    }
	/*
		STEP 3.
		Create a function that will Authenticate a user
	*/

	const login = (e) => {
		e.preventDefault()
		/*
			STEP 5.
			import the loginMutation, use it for the user Login. 
			export the mutation using graphql()
			Pass the states on the variables property
		*/

		props.loginMutation({
            variables: {
                email: email,
                password: password
            }
        }).then((response) => {
            // console.log(response)
            /*
				STEP 6.
				We'll save our credentials to localStorage. 
				After the loginMutation successfully authenticates our credentials, we get our data from the response and store it to localStorage. This will serve as our 'session'.
            */
            let data = response.data.login

            if (data != null) {
                localStorage.setItem('name', data.name)
                localStorage.setItem('id', data.id)
                localStorage.setItem('email', data.email)
                localStorage.setItem('userName', data.userName)
                localStorage.setItem('password', data.password)
                localStorage.setItem('token', data.token)
	            // console.log(localStorage)
	   			//we will be redirected to Products Page
	            setIsRedirected(true)
            } else {
                Swal.fire({
                    title: 'Login Failed',
                    text: 'Either your email or password is incorrect, please try again.',
                    type: 'error'
                })
            }
        })
	}


	/*
		STEP 1.
		Create the login form
		Make sure to import all the modules and necessary components that we need for login page
	*/

	/*
		STEP 4. 
		Apply the login() function the form onSubmit event
		Apply the states on the input fields

	*/
	return(

		<React.Fragment>

		<div className="container-fluid" id={styles.login}>
			<a href="/" style={{color: 'black', fontSize: '1.1rem'}}><i className="fas fa-angle-double-left"></i> Back</a>
			<div className="row">
				<div className="col-md-4 offset-md-2">

					<h2 className={styles.loginheading}>Login</h2>
					<form onSubmit={(e)=> login(e)}>
						<div className="form-group">
							Email
							<input
								required
								value={email}
								onChange={(e)=> setEmail(e.target.value)}
								name="fullname"
								type="email" 
								className="form-control"/>
						</div>
						<div className="form-group">
							password
							<input 
								required
								value={password}
								onChange={(e)=> setPassword(e.target.value)}
								name="password"
								type="password" 
								className="form-control"/>
						</div>
						<button className="btn btn-block btn-light border border-dark rounded-pill">Login</button>

						<p className="text-center my-2">Need an account?</p>
						<p className="text-center">click <a href="/register">here</a> to Register</p>
					</form>
				</div>
			</div>
		</div>

		<Footer/>
		</React.Fragment>
		)
}

export default graphql(loginMutation, {name: 'loginMutation'})(LoginPage)