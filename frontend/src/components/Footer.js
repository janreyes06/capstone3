import React from 'react'
// import ReactDOM from 'react-dom'
import styles from './mystyle.module.css'

const Footer = () => {
	return(
		<div className="container-fluid" style={{padding: '0'}}>	
			<div className="row" id={styles.footer}>
				<div className="col-md-8 mx-auto">
					<h2 className={styles.footerheading}>Follow me on:</h2>
					<p className="text-center">
						<a className="mx-1" target="_blank" href="https://www.facebook.com/janpatrick.reyes" style={{fontSize: '2rem', color: 'black'}}><i className="fab fa-facebook-square"></i></a>
						<a className="mx-1" target="_blank" href="https://www.linkedin.com/in/jan-patrick-reyes-0aa35b1a4/" style={{fontSize: '2rem', color: 'black'}}><i className="fab fa-linkedin"></i></a>
					</p>
					<h2 className={styles.footerheadingmail}>Email me at:</h2>
					<p className="text-center">janpat06.jpr@gmail.com</p>
					<hr/>
					<p className="text-center">This website is for educational purposes only | JPreyes Photography &copy;2020 All Rights Reserved</p>
				</div>
			</div>
		</div>
	)
}

export default Footer