import React, {useState} from 'react'
// import ReactDOM from 'react-dom'
import styles from './mystyle.module.css'

const TransactionAdd = ({addTransaction})=>{

	const [eventDate, setEventDate] = useState('')

	const transaction = {
		eventDate: eventDate
	}

	return(
		<div className="row" style={{marginBottom: '5%'}}>
			<div className="col-lg-4 offset-lg-2 col-md-6 offset-md-3">
				
				  		<h2 className={styles.bookheading}> Schedule Appointment </h2>
				 
				    	<form onSubmit={
				    			function(e){
				    				addTransaction(e, transaction)
				    			}	
				    		
				    	}>
						  <div className="form-group">
						    <label htmlFor="inputEventDate">Event Date</label>
						    <input
						    	value={eventDate} 
						    	onChange={(event)=> setEventDate(event.target.value) } 
						    	type="date" 
						    	name="eventDate" 
						    	className="form-control" 
						    	id="inputEventDate"
						    	placeholder="e.g. December 25 2020" 
						    	aria-describedby="emailHelp"
						    	required/>
						  </div>

						  <button className="btn btn-block btn-light border border-dark rounded-pill">Submit</button>
						</form>
				  
				
			</div>
		</div>
	)
}

export default TransactionAdd