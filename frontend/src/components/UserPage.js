import React from 'react'
import {graphql} from 'react-apollo'
import styles from './mystyle.module.css'
import UserList from './UserList'
import AppNavBar from './AppNavBar'
import Footer from './Footer'
import {getSpecificUser} from '../graphql/queries'
import {flowRight as compose} from 'lodash'

const UserPage = (props) => {

	let id = localStorage.getItem('id')
	// console.log(id)

	
	let user = props.getSpecificUser
	// console.log(user.user)

	return(
		<React.Fragment>

			<AppNavBar/>
			<h2 className={styles.userpageheading}>My Information</h2>
			<div className="container-fluid" style={{marginTop: '5%', marginBottom: '17%'}}>
				<UserList user={user.user}/>
			</div>

			<Footer/>

		</React.Fragment>
	)
}

export default compose(
	graphql(getSpecificUser, {
		options: (props)=>{
			return{
				variables: {
					id: props.match.params.id
				}
			}
		},

		name: 'getSpecificUser'
	}))(UserPage)