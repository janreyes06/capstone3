import React from 'react'
import styles from './mystyle.module.css'
import Swal from 'sweetalert2'

import AdminList from './AdminList'
import Footer from './Footer'
import AppNavBar from './AppNavBar'

import {graphql} from 'react-apollo'
import {flowRight as compose} from 'lodash'

import {getUsersQuery} from '../graphql/queries'
import {deleteUserMutation} from '../graphql/mutations'

const AdminPage = (props) => {

	const data = props.getUsersQuery

	const deleteUser = (id)=>{

		props.deleteUserMutation({
			variables: {id: id},
			refetchQueries: [{query: getUsersQuery}]
		})
		Swal.fire({
		                title: 'Success!',
		                text: "A user has been deleted",
		                type: 'success'
		            })
	}

	return(
		<React.Fragment>
			<AppNavBar/>
			<h2 className={styles.adminheading}>Registered Users</h2>
			<div className="container" style={{marginBottom: '10%'}}>
				<AdminList users={data.users} deleteUser={deleteUser}/>
			</div>

			<Footer/>
		</React.Fragment>
	)
}

export default compose(
	graphql(getUsersQuery, {name: 'getUsersQuery'}),
	graphql(deleteUserMutation, {name: 'deleteUserMutation'})
)(AdminPage)