import React from 'react'
import {Link} from 'react-router-dom'

const UserRow = (props)=>{
	// console.log(props.user.id)
	return(
		<React.Fragment>
			<h4>Name:</h4>
			<p>{props.user.name}</p>

			<h4>Email:</h4>
			<p>{props.user.email}</p>

			<h4>Username:</h4>
			<p>{props.user.userName}</p>

			<h4>Password:</h4>
			<p>*********</p>

			<Link to={"/user/update/" + props.user.id}>
				<button className="btn btn-light btn-block rounded-pill border border-dark">Edit</button>
			</Link>
		</React.Fragment>
	)
}

export default UserRow