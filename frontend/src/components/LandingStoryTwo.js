import React from 'react'
// import ReactDOM from 'react-dom'
import styles from './mystyle.module.css'

const LandingStoryTwo = () => {
	return(
		<div className="container" data-aos="fade-left">
			<div className="row" id={styles.storytwo}>
				<div className="col-md-4 offset-md-4">
					<p className={styles.storytwopara}>"the most awaited day of my life is also the most exciting thing to remember. I'm really grateful that JPreyes did a fantastic job working in our photos!"</p>
					<p className={styles.storytwopara2}>"contact him now and I promise you that it will be worth it!"</p>
				</div>
			</div>
		</div>
	)
}

export default LandingStoryTwo