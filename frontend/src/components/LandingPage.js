import React from 'react'
// import ReactDOM from 'react-dom'
import styles from './mystyle.module.css'

import LandingAbout from './LandingAbout'
import LandingStoryOne from './LandingStoryOne'
import LandingStoryTwo from './LandingStoryTwo'
import LandingStoryThree from './LandingStoryThree'
import LandingParallax from './LandingParallax'
import Footer from './Footer'

const LandingPage = () => {
	return(
		<React.Fragment>
		
			<div className="container-fluid">
				<div className="row" id={styles.landing}>
					<div className="col-md-8 mx-auto text-center">
						<h1 className={styles.landingheading}><span data-aos="fade-right" style={{display: 'inline-block'}}>JP</span>reyes Photography</h1>
						<p className={styles.landingquote}>“when people ask me what equipment I use – I tell them my eyes.”</p>
						<p>
							<a href="/register" className="btn btn-light border border-dark rounded-pill">Book Now</a>
						</p>
					</div>
				</div>
			</div>

			<LandingAbout/>
			
			<LandingStoryOne/>

			<LandingStoryTwo/>

			<LandingStoryThree/>
			
			<LandingParallax/>

			<Footer/>

		</React.Fragment>
	)
}

export default LandingPage