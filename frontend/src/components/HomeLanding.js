import React from 'react'
// import ReactDOM from 'react-dom'
import styles from './mystyle.module.css'

import AppNavBar from './AppNavBar'
import Footer from './Footer'
import HomeAbout from './HomeAbout'
import HomeStoryOne from './HomeStoryOne'
import HomeStoryTwo from './HomeStoryTwo'
import HomeStoryThree from './HomeStoryThree'
import HomeParallax from './HomeParallax'

const HomeLanding = () => {
	// console.log(localStorage)
	return(
		<React.Fragment>

			<AppNavBar/>	

			<div className="container-fluid">
				<div className="row" id={styles.homelanding}>
					<div className="col-md-8 mx-auto text-center">
						<h1 className={styles.homeheading}>JPreyes Photography</h1>
						<p className={styles.landingquote}>“when people ask me what equipment I use – I tell them my eyes.”</p>
						<p>
							<a href="/transaction" className="btn btn-light border border-dark rounded-pill">Book Now</a>
						</p>
					</div>
				</div>
			</div>

			<HomeAbout/>

			<HomeStoryOne/>

			<HomeStoryTwo/>
			
			<HomeStoryThree/>

			<HomeParallax/>

			<Footer/>

		</React.Fragment>
	)
}

export default HomeLanding