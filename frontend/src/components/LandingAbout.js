import React from 'react'
// import ReactDOM from 'react-dom'
import styles from './mystyle.module.css'

const LandingAbout = () => {
	return(
		<div className="container-fluid">
			<h2 className={styles.aboutheading}>About me</h2>
			<div className="row" id={styles.landingabout}>
				<div className="col-md-4 offset-md-1">
					<h2 className={styles.aboutgreeting}>Hello! I'm JP Reyes</h2>
					<p className={styles.aboutpara}>My style has been described as creative, fun, engaging, and relaxed. I specialize
					in weddings and event photography.</p>
					<p className={styles.aboutpara}>I look forward to meeting you and discussing your photography needs.</p>
				</div>
			</div>
		</div>
	)
}

export default LandingAbout