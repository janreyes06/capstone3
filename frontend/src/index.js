import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import LandingPage from './components/LandingPage'
import HomeLanding from './components/HomeLanding'
import RegisterPage from './components/RegisterPage'
import LoginPage from './components/LoginPage'
import TransactionPage from './components/TransactionPage'
import TransactionUpdatePage from './components/TransactionUpdatePage'
import AdminPage from './components/AdminPage'
import UserPage from './components/UserPage'
import UserUpdatePage from './components/UserUpdatePage'


import ApolloClient from 'apollo-boost'
import { ApolloProvider } from 'react-apollo'

const client = new ApolloClient({ uri: 'http://localhost:4000/graphql' })

const pageComponent = (

	<React.Fragment>
		<ApolloProvider client={ client }>
		<BrowserRouter>
			<Switch>
				<Route exact path="/" component={LandingPage}/>
				<Route exact path="/home" component={HomeLanding}/>
				<Route exact path="/register" component={RegisterPage}/>
				<Route exact path="/login" component={ LoginPage }/>
				<Route exact path="/transaction" component={ TransactionPage }/>
				<Route exact path="/transaction/update/:transactionId" component={TransactionUpdatePage}/>
				<Route exact path="/admin" component={ AdminPage }/>
				<Route exact path="/user/:id" component={ UserPage }/>
				<Route exact path="/user/update/:id" component= { UserUpdatePage }/>
			</Switch>
		</BrowserRouter>
		</ApolloProvider>
	</React.Fragment>

)



ReactDOM.render( pageComponent, document.getElementById('root'));


