const express = require('express')
const app = express()
const mongoose = require('mongoose')
const cors = require('cors')


/*for graphql playground*/
const graphqlHTTP = require('express-graphql')
const graphqlSchema =  require('./gql-schema/gql-types-queries-mutations')









/*Database connection*/
// mongoose.connect('mongodb://localhost:27017/capstone3', {
// 	useNewUrlParser:true,
// 	useUnifiedTopology: true,
// 	useFindAndModify: false
// })
// mongoose.connection.once('open', () => {
// 	console.log("Now connected to Local MongoDB Server")
// })

// mongoose
//   .connect(
//     `mongodb+srv://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@cluster0-4dd4k.gcp.mongodb.net/${process.env.MONGO_DB}?retryWrites=true&w=majority`, mongooseOptions
//   )
//   .then(() => {
//     app.listen(PORT,()=>{
//       console.info(`MongoDB Connected!`);
//       console.log(`CORS-enabled web server listening on port ${PORT}`); 
//       console.info(`Listening to PORT: ${PORT}`);
//     });
//   })
//   .catch(err => {
//     console.log(err);
// });

mongoose.connect('mongodb+srv://janpatrickreyes:petwussy21@cluster0-a7m7t.mongodb.net/capstone3?retryWrites=true&w=majority', {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useFindAndModify: false
})
mongoose.connection.once('open', () => {
	console.log('Now Connected to MongoDB atlas Server')
})




app.use(cors())

/*gql playground*/
app.use('/graphql', graphqlHTTP({schema: graphqlSchema, graphiql:true}))


/*Server initialization*/
app.listen(4000, ()=>{
	console.log('Now serving on port 4000')
})
